var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.get('/room', function (req, res) {
	res.sendFile(__dirname + '/room.html');
});


var rooms = {};
var usernames = {};

io.on('connection', function (socket) {

	socket.on('sendchat', function (msg) {
		io.sockets.in(socket.room).emit('updatechat', socket.username, msg);
	});

	socket.on('adduser', function (username, room) {
		socket.username = username;
		socket.room = room;

		if (rooms[room] == undefined) {
			rooms[room] = room;
		}

		usernames[username] = room;

		socket.join(room);
		socket.emit('updatechat', 'SERVER', 'you have connected to ' + room);
		socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', username + ' has joined');

		socket.emit('updateCurrentRoom', room);
		io.sockets.emit('updateusers', usernames);
		io.sockets.emit('updaterooms', rooms);
		
	});

	socket.on('switchRoom', function (newroom) {
		socket.leave(socket.room);
		socket.join(newroom);
		socket.emit('updatechat', 'SERVER', 'you have connected to ' + newroom);
		socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' has left the room');

		usernames[socket.username] = newroom;

		socket.room = newroom;
		socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username + ' has joined');

		
		socket.emit('updateCurrentRoom', newroom);
		io.sockets.emit('updateusers', usernames);
		io.sockets.emit('updaterooms', rooms);
	});

	socket.on('disconnect', function () {
		delete usernames[socket.username];
		io.sockets.emit('updateusers', usernames);
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		socket.leave(socket.room);
	});
});

http.listen(3000, function () {
	console.log('listening on *:3000');
});


